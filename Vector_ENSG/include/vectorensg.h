#ifndef VECTORENSG_H
#define VECTORENSG_H

#include "stdlib.h"

class VectorENSG
{
public:
    /**
     * \brief Constructs a vector from its size
     *
     * \param size Size of the vector
     */
    VectorENSG(size_t size);

    /**
     * \brief Constructs a vector from another one (copy constructor)
     *
     * \param v Vector to be copied
     */
    VectorENSG(const VectorENSG & v);
    ~VectorENSG();

    VectorENSG & operator=(const VectorENSG & v);
    void operator=(float value);
    float operator[](size_t i) const;
    float & operator[](size_t i);
    bool operator==(const VectorENSG & v) const;

    /**
     * \brief Computes the sum of two vectors
     *
     * \param v Vector to be summed
     * \return Sum of this vector and v
     */
    VectorENSG operator+(const VectorENSG & v) const;
    VectorENSG operator-(const VectorENSG & v) const;
    size_t getSize() const;

private:
    size_t m_size;
    float * m_vector;
};

#endif // VECTORENSG_H
